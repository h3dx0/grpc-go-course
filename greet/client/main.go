package main

import (
	"log"

	pb "github.com/robertonunezc/grpc-go-course/greet/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var addr string = "localhost:50051"

func main() {
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("Fatal to connect: %v\n", addr)
	}
	defer conn.Close()
	client := pb.NewGreetServiceClient(conn)
	GreetLong(client)
}
