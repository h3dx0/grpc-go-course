package main

import (
	"context"
	"log"

	pb "github.com/robertonunezc/grpc-go-course/greet/proto"
)

func doGreet(client pb.GreetServiceClient) {
	log.Println("Starting to do a Greet RPC...")
	req := &pb.GreetRequest{
		FirstName: "Roberto",
	}
	res, err := client.Greet(context.Background(), req)
	if err != nil {
		log.Fatalf("Error calling Greet: %v", err)
	}
	log.Printf("Response from Greet: %v", res.Result)
}
