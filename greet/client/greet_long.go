package main

import (
	"context"
	"log"
	"time"

	pb "github.com/robertonunezc/grpc-go-course/greet/proto"
)

func GreetLong(c pb.GreetServiceClient) error {
	req := []*pb.GreetRequest{
		{
			FirstName: "Roberto",
		},
		{
			FirstName: "Juan",
		}, {
			FirstName: "Pepe",
		},
	}
	stream, err := c.LongGreet(context.Background())
	if err != nil {
		log.Printf("Error calling LongGreet")
		return err
	}
	for _, req := range req {
		log.Println("Sending request to LongGreet")
		stream.Send(req)
		time.Sleep(1000 * time.Millisecond)
	}
	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Printf("Error receiving response from LongGreet")
		return err
	}
	log.Printf("Response from LongGreet: %v", res)
	return nil
}
