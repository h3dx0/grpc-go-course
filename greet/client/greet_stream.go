package main

import (
	"context"
	"log"

	pb "github.com/robertonunezc/grpc-go-course/greet/proto"
)

func GreetManyTimes(c pb.GreetServiceClient) error {
	req := &pb.GreetRequest{
		FirstName: "Roberto",
	}
	stream, err := c.GreetManyTimes(context.Background(), req)
	if err != nil {
		log.Printf("Error calling GreetManyTimes")
		return err
	}
	for {
		res, err := stream.Recv()
		if err != nil {
			break
		}
		log.Printf("Response from GreetManyTimes: %v", res)
	}
	return nil
}
