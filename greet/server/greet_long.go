package main

import (
	"log"

	pb "github.com/robertonunezc/grpc-go-course/greet/proto"
)

func (s *Server) LongGreet(stream pb.GreetService_LongGreetServer) error {
	log.Printf("Calling LongGreet")
	var result string
	for {
		req, err := stream.Recv()
		if err != nil {
			break
		}
		result += "Hello " + req.FirstName + "!"
	}
	return stream.SendAndClose(&pb.GreetResponse{
		Result: result,
	})
}
