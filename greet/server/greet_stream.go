package main

import (
	"log"

	pb "github.com/robertonunezc/grpc-go-course/greet/proto"
)

func (s *Server) GreetManyTimes(req *pb.GreetRequest, stream pb.GreetService_GreetManyTimesServer) error {
	log.Printf("Calling GreetManyTimes with: %v", req)
	for i := 0; i < 10; i++ {
		stream.Send(&pb.GreetResponse{
			Result: "Hello " + req.FirstName,
		})
	}
	return nil
}
