package main

import (
	"context"
	"log"

	pb "github.com/robertonunezc/grpc-go-course/greet/proto"
)

func (s *Server) Greet(ctx context.Context, req *pb.GreetRequest) (*pb.GreetResponse, error) {
	log.Printf("Calling Greet with: %v", req)
	return &pb.GreetResponse{
		Result: "Hello " + req.FirstName,
	}, nil
}
